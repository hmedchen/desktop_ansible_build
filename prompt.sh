PROMPT_COMMAND=assemble_prompt
# CONFIG
PROMPT_DIRTRIM=2

#Retval modes: 1=UTF-8 Emojis, 2=ASCII emoji, 2=Retval
RETVAL_MODE=2

#Git modes: 0=branch and status; 2=branch only
GIT_MODE=1

function nonzero_return {
      RETVAL=$?
      case $RETVAL_MODE in
	1)
	  if [ $(locale charmap) == 'UTF-8' ]
	  then
    	    if [ $RETVAL -ne 0 ]; then
		    if [ $RETVAL -ne 130 ]; then
			    echo $'\xf0\x9F\x98\x96'" "
		    else
			    echo $'\xf0\x9F\x98\x91'" "
		    fi
	    fi
          else
    	    [ $RETVAL -ne 0 ] && echo ${RETVAL}
	  fi
	  ;;
	2)
          [ $RETVAL -ne 0 ] && echo "( -.-)"
	  ;;
	*)
	  [ $RETVAL -ne 0 ] && echo "$RETVAL"
       esac
}

# get current branch in git repo
function parse_git_branch {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		if [ $GIT_MODE -eq 1 ]; then
	  	  STAT=`parse_git_dirty`
		else
		  STAT=""
		fi
		echo "[${BRANCH}${STAT}]"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

function virtualenv_info()
{
	if [[ -n $VIRTUAL_ENV ]]; then
		venv="${VIRTUAL_ENV##*/}"
	else
		venv=''
	fi
	[[ -n "$venv" ]] && echo "\[\e[33m\]($venv)"
}

function assemble_prompt
{
	#venv=$(virtualenv_info)
	retval=$(nonzero_return)
	if [[ "$vbox" != "yes" ]]
	then
		hostcol="\[\e[1;33m\]"
	else
		hostcol=''
	fi

	export PS1="$(virtualenv_info)\[\e[32m\]\u@${hostcol}\h\[\e[m\]:\[\e[36m\]\w\[\e[m\]\[\e[34m\]$(parse_git_branch)\[\e[m\]\[\e[31m\]${retval}\[\e[m\]\\$ "
}

