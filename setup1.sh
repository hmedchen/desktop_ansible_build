#!/usr/bin/env bash

echo "------- prereqs --------"
echo "VMWARE bidirectual copy/paste setup"
echo "VMware guest additions installed"
echo "> sudo apt install git"
echo "reboot to be able to copy/paste"

echo "------- initial package install --------"
sudo apt install git ansible

echo "------- creating ssh keypair --------"
if [ ! -f ~/.ssh/id_rsa.pub ]; then
   ssh-keygen
else
   echo "ssh key already exists, skipping"
fi
echo "------- public key. enter that in your bitbucket ssh keyring --------"
cat ~/.ssh/id_rsa.pub


