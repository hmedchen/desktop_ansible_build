#!/ama/nsdboard/PRD/PYTHON_Modules/python/bin/python

import MySQLdb
import logging
import sys
import os
import getopt
import MySQLdb.cursors
import datetime
import urllib2
import csv
import json
import sqlite3

# GLOBALS
WIPFilePrefix="wip_"
WIPFileSuffix=".csv"
WIPTableName="wip"
WIPLocation="http://mucosit022/sprint/wip/"

PUTFilePrefix="put_"
PUTFileSuffix=".csv"
PUTTableName="put"
PUTLocation="http://mucosit022/sprint/put/"

CKWIPFilePrefix="ckwip"
CKWIPFileSuffix=".csv"
CKWIPTableName="ckwip"
CKWIPLocation="http://mucosit022/nseydel/"

SQLiteFile=os.environ["SQLITE_PATH"]+"reporting.sqlite3"

def ConnectDB():
  global SQLiteFile
#  MYSQL_USER='helmut'
#  MYSQL_DB='wip'
#  MYSQL_PWD='helmut'
#  MYSQL_HOST='localhost'
#  MYSQL_PORT=3306
#    db=MySQLdb.connect (host=MYSQL_HOST, port=MYSQL_PORT, user=MYSQL_USER ,passwd=MYSQL_PWD, db=MYSQL_DB, cursorclass=MySQLdb.cursors.DictCursor)

  db = sqlite3.connect(SQLiteFile)
  return (db)

def getInsertLine (CSVArray):
  line=[]
  row=''
  #    print CSVArray;
  
  if (type(CSVArray) == dict):
    for key,value in CSVArray.items():
      value=value.encode('string-escape').replace('"', "\'")
      if (value == ''):
        value="NULL"
      elif (value.isdigit() == False):
        value='"'+value+'"'
      if (row != ''):
        row=",".join([row,value])
      else:
        row=value
  else:
    for item in CSVArray:
      item=item.encode('string-escape').replace('"',"\'")
      if (item == ''):
        item="NULL"
      elif (item.isdigit() == False):
        item='"'+item+'"'
      if (row != ''):
        row=",".join([row,item])
      else:
        row=item
  return (row)

def CreateCKWIPTable (db):
  SQL="CREATE TABLE `ckwip` ( `ck` int(11) NOT NULL, `severity` int(11) DEFAULT NULL, `KPI` varchar(16) DEFAULT NULL, `Request` varchar(16) DEFAULT NULL, `function` varchar(20) DEFAULT NULL, `Message` varchar(100) DEFAULT NULL, `Detail` varchar(100) DEFAULT NULL, `Name1` varchar(50) DEFAULT NULL, `value1` varchar(50) DEFAULT NULL, `Name2` varchar(50) DEFAULT NULL, `value2` varchar(50) DEFAULT NULL, `OwnedBy` varchar (50) DEFAULT NULL, `Name` varchar (50) DEFAULT NULL, MR varchar (15) DEFAULT NULL, PRIMARY KEY (`ck`));"

  cursor = db.cursor ()
  print "creating table CKWIP"  
  try:
     cursor.execute (SQL)
     cursor.execute ("create index xxx on ckwip(kpi);")
     cursor.execute ("create index yyy on ckwip(Message);")
     print "done"
  except:
     print "Can't create table, does it exist? (check to be improved)"

def CreateCKAproachTable (db):
  SQL="CREATE TABLE `aproach` ( `LOGGED_DATE` date DEFAULT NULL, `PURPOSE` varchar(100) DEFAULT NULL, `WORKTYPE_CLASS` varchar(100) DEFAULT NULL, `PEGA_WORKOBJECT` varchar(12) DEFAULT NULL, `RNID` int  NOT NULL, `REC_TITLE` varchar (150) DEFAULT NULL, `STATUS` varchar (100) DEFAULT NULL, `NB_OF_HOPS` int DEFAULT NULL, `FIRST_ASSIGNEE` varchar (150) DEFAULT NULL, `RETURN_DATE` date DEFAULT NULL, `TASK_CATEGORY` varchar (50), `RUNTIME` float);"

  cursor = db.cursor ()
  print "creating table APROACH"  
  try:
     cursor.execute (SQL)
     cursor.execute ("create index zzz on aproach(PEGA_WORKOBJECT);")
     print "done"
  except:
     print "Can't create table, does it exist? (check to be improved)"


#------------------------------------------------------------------------------------------
# Import CSV File
# TODO: SmartInsert checks each row and inserts only the updates on a given key
#------------------------------------------------------------------------------------------
def ImportCSVFile (db, RemoteFileName, TableName, SmartInsert=False):
  str=","
  InsertData=''
  lines=[]

  if (isinstance(SmartInsert,basestring)):
    Smart=True
    SmartInsert=SmartInsert.strip()
    print "Trying smart insert on column "+SmartInsert
  else:
    Smart=False
    print "Trying truncate and insert"
    
  fp=urllib2.urlopen(RemoteFileName)
  cursor = db.cursor ()

  if Smart == False: 
    # Get rid of header line
    columns=fp.readline()
    #print columns
    RemoteCSVFile = csv.reader(fp,  delimiter=',', quotechar='"')
    for CSVArray in RemoteCSVFile:
      line=getInsertLine(CSVArray)
      lines.append("("+line.strip()+")\n")
    try:
      try:
        print "Trying to truncate table"
        # clean old table
        print "delete from "+TableName
        cursor.execute ("delete from "+TableName+";")
      except sqlite3.OperationalError as err:
        print "Error Truncating - continuing"+format(err)
      print "select count(*) count from "+TableName
      cursor.execute ("select count(*) count from "+TableName+";")
      row = cursor.fetchone ()
      #print row[0]
      InsertData=str.join(lines)
      print "insert data"
      # import data into the table
      # can't use that as the WIP excel has dupe column names
      for line in lines:
        SQL_Insert="INSERT INTO "+TableName+" VALUES "+line+";"
        #print SQL_Insert
        cursor.execute (SQL_Insert)
      print "committing"
      db.commit()
    except sqlite3.OperationalError as err:
      #_mysql_exceptions.OperationalError
      print("Something went wrong: {}"+format(err)) 
  else:
    columns=fp.readline().split(',')
    print columns
    RemoteCSVFile = csv.reader(fp,  delimiter=',', quotechar='"')
    CheckColumnNumber=columns.index(SmartInsert) 
    for CSVArray in RemoteCSVFile:
      # Check for matching column
      SQL_getKey="SELECT count(*) count from "+TableName+" where "+SmartInsert + " = '"+CSVArray[CheckColumnNumber]+"'"
      cursor.execute (SQL_getKey)
      count=cursor.fetchone()['count']
      if (count == 0):
#        print "Key not found yet, inserting..." 
        InsertLine="Insert into "+TableName+" values ("+getInsertLine(CSVArray)+")"
        cursor.execute (InsertLine)
#        db.commit()
      elif (count == 1):
#        print "Key found, deleting/inserting.."
        cursor.execute ("Delete from "+TableName+" where "+SmartInsert + " = '"+CSVArray[CheckColumnNumber]+"'")
        InsertLine="Insert into "+TableName+" values ("+getInsertLine(CSVArray)+")"
        cursor.execute (InsertLine)
#        db.commit()
      else:
        print "Key is not unique, exitting!"
        return (1)
      
     
  # validating row count
  cursor.execute ("select count(*) from "+TableName)
  row = cursor.fetchone ()
  print row[0]
  
  fp.close()
#  print InsertData;
   
  
#  print row
  cursor.close ()
  
#------------------------------------------------------------------------------------------
# MAIN
#
#------------------------------------------------------------------------------------------
def main (argv):
  global WIPFilePrefix
  global WIPFileSuffix
  global WIPTableName
  global WIPLocation

  global PUTFilePrefix
  global PUTFileSuffix
  global PUTTableName
  global PUTLocation

  global CKWIPFilePrefix
  global CKWIPFileSuffix
  global CKWIPTableName
  global CKWIPLocation
  # open DB connection
  db=ConnectDB();

  try:
    opts, args = getopt.getopt (argv,"d:",["date="])
  except getopt.GetoptError:
    print "Usage: ImportSMTData.py [-d date(%Y-m-d)]"
    sys.exit(2)
  for opt, arg in opts:
    if opt in ('-d', "--date"): 
      TargetDate = datetime.datetime.strptime(arg, '%Y-%m-%d')
 
  
  try:
    TargetDate
  except NameError: 
    TargetDate = datetime.date.today()
  # get date of source files - should be every Sunday's date

  print TargetDate


  FileDate=TargetDate + datetime.timedelta(days=-TargetDate.weekday()-1)  
  
  # reading WIP source file
#  print "----- WIP ----"
#  WIPFileName=WIPLocation + WIPFilePrefix + FileDate.strftime("%Y%m%d") + WIPFileSuffix
#  WIPFile=ImportCSVFile (db, WIPFileName, WIPTableName)  
 
  # reading PUT source file
#  print "----- PUT ----"
#  PUTFileName=PUTLocation + PUTFilePrefix + FileDate.strftime("%Y%m%d") + PUTFileSuffix
#  PUTFile=ImportCSVFile (db, PUTFileName, PUTTableName, 'Id')  
  
  # reading CKWIP source file
  print "----- CKWIP ----"
  CreateCKWIPTable(db)
  CreateCKAproachTable(db) # only create, will be filled via different script
  CKWIPFileName=CKWIPLocation + CKWIPFilePrefix + CKWIPFileSuffix
  CKWIPFile=ImportCSVFile (db, CKWIPFileName, CKWIPTableName)  

  # close DB connection
  db.close()

if __name__ == "__main__":
  main(sys.argv[1:])


