#!/usr/bin/env bash

if [ -e /ama/nsdboard/PRD/scripts/sprintReport/scripts/.PRD ]
then
  . /ama/nsdboard/PRD/env.sh
  cd /ama/nsdboard/PRD/scripts/sprintReport/scripts
  export SQLITE_PATH="/ama/nsdboard/PRD/data/sprintReport/"
else
  export SQLITE_PATH="../db/"
fi
echo "SQLITE PATH: ${SQLITE_PATH}"

function GetSQLiteDB
{
  echo "moving reporing.sqlite3 to backup"
  mv ${SQLITE_PATH}reporting.sqlite3 ${SQLITE_PATH}reporting.bkp.sqlite3
  echo "fetching new file"
  time wget http://mucosit022/sprint/etc/reporting.sqlite3 -P ${SQLITE_PATH}
  if [ $? != 0 ]
  then
    echo "wget failed, restoring old file"
    cp ${SQLITE_PATH}reporting.bkp.sqlite3 ${SQLITE_PATH}reporting.sqlite3
  else
    echo "wget successful"
  fi
  ls -l ${SQLITE_PATH}reporting*
}

# getting SQLite DB
time GetSQLiteDB 2>&1

# converting the view_server view into a table to boost performance
time ./ConvViewToTable.py 2>&1

# Trying to load the ckwip data
time ./ImportSMTData.py  2>&1

# Trying to load the aproach data
time ./AllAproach.py 2>&1

# Trying to create the static DB if it doesn't exist yet
time ./CreateStaticDB.py 2>&1

# Trying to load the Operational Criteria Tables
time ./ImportOPCriteriaData.py 2>&1

# Getting status of all IREQs (first try)
time ./GetIREQStatus.py 2>&1
