#!/usr/bin/env python

###############################################################################
#
# Team              : TSL App. Mgmt.
#
# Author            : Ben Schwalb (ben.schwalb@amadeus.com)
#
# File              : topocsv.py
#
# Description       : Output data from topology DB in CSV format. It can also
#                     be used as code for other scripts to process the data.
#
# Copyright (C) 2014 AMADEUS. All rights reserved.
#
###############################################################################

#Thanks! http://developer.yahoo.com/python/python-xml.html

from sys import argv
from getopt import gnu_getopt, GetoptError
import urllib
from xml.dom import minidom
from collections import namedtuple

#CONSTANTS
BASE_URL = ('http://topology.muc.amadeus.net:8080/'
            'topodb/service/application-phase')
NODE_TAG = 'host'
NODE_NAME_TAG = 'hostname'
TYPE_TAG = 'type'
APPPHASE_TAG = 'applicationPhase'
APP_TAG = 'name'
PHASE_TAG = 'phase'
SWPKG_TAG = 'swPackage'

#TODO If no arguments, just get everything.
#TODO Filter on types?
#TODO applicationType=OBE or TSL in URL query list if apps are not specified?
#TODO Throw HTTP errors after mucimp02 migration with new python version?

#TODO getApps function to return a list of all the apps

class AppNode( namedtuple('AppNode',
              ['node', 'type', 'cluster', 'peak', 'app', 'phase']) ):
    def __str__(self):
        return ','.join(self)

def __main__(args):

    try:
        opts, _ = gnu_getopt(args[1:], "a:p:n:h", ["help"]) 
    except GetoptError, err:
        print str(err)

    apps = phases = nodes = []

    for (opt, arg) in opts:
        if opt == "-a":
            apps = arg.split(',')
        elif opt == "-p":
            phases = arg.split(',')
        elif opt == "-n":
            nodes = arg.split(',')
        elif opt == "-h" or "--help":
            printHelpMessage()
            return

    if len(nodes) > 0:
        app_phase_list = getNodeApps(nodes)
    elif len(apps)==0 or len(phases)==0:
        appPhaseList = getAppPhases(apps, phases)
    else:
        app_phase_list = [(a, p) for a in apps for p in phases]

    for (app, phase) in app_phase_list:
        for i in getApp(app, phase):
            if len(nodes)<=0 or i.node in nodes:
                print i

def getAppPhaseURL():
    return BASE_URL

#Either apps or phases list must be empty to get expected result
def getAppPhaseTags(apps, phases):
    return filter(lambda x: x.getAttribute(APP_TAG) in apps or x.getAttribute(PHASE_TAG) in phases, getAppPhaseXML(apps, phases).getElementsByTagName(APPPHASE_TAG))

def getAppPhases(apps, phases):
    return [(x.getAttribute(APP_TAG), x.getAttribute(PHASE_TAG)) for x in getAppPhaseTags(apps, phases)]

def getAppPhaseXML(apps, phases):
    return getXML(getAppPhaseURL())

def getApp(app, phase):
    return getAppHosts(getAppXML(app, phase), app, phase)

def getAppHosts(xml, app, phase):
    return [AppNode( app=app, phase=phase, *x ) for x in getHosts(xml)]

#TODO Follow-up why the API does not work for shared nodes
def getNodeApps(nodes):
    output = []
    for node in nodes:
        nodeXML = getXML('http://topology.muc.amadeus.net:8080/topodb/service/server?hostname='+node)
        for i in nodeXML.getElementsByTagName("softwareServer"):
            split = i.getAttribute("split")
            splitXML = getXML(split)
            for j in splitXML.getElementsByTagName("split"):
                output.append ((j.getAttribute("applicationName"),
                j.getAttribute("phaseName")))
    return output

#TODO Follow-up why databases (apfprd) do not have node names and are type oltp
#Output: tuple (machine,type,cluster). cluster is empty string if doesn't exist
def getHosts(xml):
    return [
            (
                x.getAttribute(NODE_NAME_TAG),
                x.parentNode.parentNode.getAttribute(TYPE_TAG) or x.parentNode.getAttribute(SWPKG_TAG)+" "+x.parentNode.getAttribute(TYPE_TAG),
                x.parentNode.getAttribute(NODE_NAME_TAG),
                x.parentNode.parentNode.parentNode.getAttribute("name") or x.parentNode.parentNode.parentNode.parentNode.getAttribute("name")
            )
            for x in xml.getElementsByTagName(NODE_TAG)
            ]

def getURL(app, phase):
    return BASE_URL+'?%s' % urllib.urlencode({'application':app, 'phase':phase})

def getAppXML(app, phase):
    return getXML(getURL(app, phase))

def getXML(url):
    conn = urllib.urlopen(url)
    xml = minidom.parse(conn)
    conn.close()
    return xml

#TODO Make app and phase args optional once the code is changed so they are
def printHelpMessage():
    print "A script to get data in csv format from Topology DB"
    print "Usage: "+argv[0]+" [-a <app>[,<app2>]... |  -p <phase>[,<phase2>]...]"
    
#The following is only run if script is executed directly
#If another module imports this module, it isn't run
if __name__ == "__main__":
    __main__(argv)
