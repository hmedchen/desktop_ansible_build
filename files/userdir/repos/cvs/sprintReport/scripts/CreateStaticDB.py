#!/ama/nsdboard/PRD/PYTHON_Modules/python/bin/python

import logging
import sys
import os
import getopt
import datetime
import sqlite3

# GLOBALS

SQLiteFile=os.environ["SQLITE_PATH"]+"static_reporting.sqlite3"
SQLiteFileMainDB=os.environ["SQLITE_PATH"]+"reporting.sqlite3"

def ConnectDB():
  global SQLiteFile
  db = sqlite3.connect(SQLiteFile)
  return (db)


def CreateStaticTables (db):
  cursor = db.cursor ()

  SQL_T1="CREATE table IREQ_comment (IREQ varchar(12),comment text);" 
  SQL_I1="CREATE INDEX 'i_IREQ' on IREQ_comment (IREQ ASC);"
  SQL_T2="CREATE table ckwip_stat (collect_date date, correct int, in_doubt int, message varchar(50),ownedby varchar(20), min_age int, avg_age int, max_age int);" 
  SQL_I2="CREATE INDEX 'i_date' on ckwip_stat (collect_date);"
  SQL_I3="CREATE INDEX 'i_owner' on ckwip_stat (ownedby);"
  SQL_I4="CREATE INDEX 'i_message' on ckwip_stat (message);"
    
  try:
     print "creating static tables (IREQ_comment);"
     cursor.execute (SQL_T1)
     print "done"
  except sqlite3.Error as e:
     print "Can't Create table. Most likely it exists already."
     print "Error: ", e.args[0]
  try:
     print "Creating some indexes..."
     cursor.execute (SQL_I1)
     print "done"
  except sqlite3.Error as e:
     print "Can't Create index. Most likely it exists."
     print "Error: ", e.args[0]
  try:
     print "creating static tables (ckwip_stat);"
     cursor.execute (SQL_T2)
     print "done"
  except sqlite3.Error as e:
     print "Can't Create table. Most likely it exists already."
     print "Error: ", e.args[0]
  try:
     print "Creating some indexes..."
     cursor.execute (SQL_I2)
     cursor.execute (SQL_I3)
     cursor.execute (SQL_I4)
     print "done"
  except sqlite3.Error as e:
     print "Can't Create index. Most likely it exists."
     print "Error: ", e.args[0]

  cursor.close()

def InsertCkwipStats (db):
  global SQLiteFileMainDB
  cursor = db.cursor ()

  try:
    print "Trying to attach main DB to stats DB"
    SQL_Attach = "Attach database '%s' as wipdb" %SQLiteFileMainDB
    print SQL_Attach
    cursor.execute (SQL_Attach)
    print "done"
  except sqlite3.Error as e:
     print "Can't Attach DB."
     print "Error: ", e.args[0]


  SQL_InsertStats="insert into ckwip_stat (collect_date, correct, in_doubt, message, ownedby, min_age, avg_age, max_age) select" \
" date('now'), (select count(view_server.id) wip from view_server where exited == '')-  	(select count(ckwip.ck) ckwip from ckwip) correct," \
" count (ck.kpi) in_doubt," \
" message," \
" ck.ownedby, " \
" round(min(julianday('now') - julianday (v.approved))) min_age," \
" round(avg(julianday('now') - julianday (v.approved))) avg_age," \
" round(max(julianday('now') - julianday (v.approved))) max_age" \
" from wipdb.ckwip ck, wipdb.view_server v" \
" where ck.kpi=v.id" \
" group by message, ck.ownedby;"

  SQL_CkDateEntryExists="SELECT count(collect_date) count from ckwip_stat where collect_date=date('now')"

  cursor.execute (SQL_CkDateEntryExists)
  count=cursor.fetchone()[0]

  if (count == 0):
  # insert stats if not existing already
    try:
      print "Inserting stats of the day"
      retval=cursor.execute (SQL_InsertStats)
      db.commit() 
      print "done"
    except sqlite3.Error as e:
      print "Can't Insert stats!"
      print "Error: ", e.args[0]
  else:
    print "today's stats already exist, exitting"





#------------------------------------------------------------------------------------------
# MAIN
#
#------------------------------------------------------------------------------------------
def main (argv):
  # open DB connection
  db=ConnectDB()

  CreateStaticTables(db)

  InsertCkwipStats(db)

  # close DB connection
  db.close()

if __name__ == "__main__":
  main(sys.argv[1:])


