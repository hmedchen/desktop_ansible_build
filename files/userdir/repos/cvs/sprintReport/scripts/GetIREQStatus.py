#!/ama/nsdboard/PRD/PYTHON_Modules/python/bin/python

import cx_Oracle
import csv
import sys
import datetime
import os
import json
import sqlite3

        ##########################################################
        #                                                        #
        # Creates new table to hold IREQ info                    #
        #                                                        #
        ##########################################################

SQLiteFile=os.environ["SQLITE_PATH"]+"reporting.sqlite3"

now = datetime.datetime.now()

date=now.strftime('%d-%b-%Y').upper()
nowDir = now.strftime('%d-%b-%Y %H%M').upper()

columns = ('LOGGED_DATE','PURPOSE','WORKTYPE_CLASS','PEGA_WORKOBJECT','RNID','REC_TITLE','PURPOSE+STATUS','NB_OF_HOPS','FIRST_ASSIGNEE','RETURN_DATE','TASK_CATEGORY','RUNTIME')

# local SQLITE
con3 = sqlite3.connect (SQLiteFile)
cur3 = con3.cursor()

print "get data from Pega"
# Pega Side

#con = cx_Oracle.connect('PEGDEP_CHECK/Dep4#REd2@PEGADB2')
con = cx_Oracle.connect('PEGDEP_CHECK/Dep4#REd2@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.24.33.177)(PORT=31180))(ADDRESS=(PROTOCOL=TCP)(HOST=172.24.33.178)(PORT=31180)))(CONNECT_DATA=(SERVICE_NAME=PEGPRDSRV)))')
cur = con.cursor()

IREQData = {}

print "creating IREQ table"
cur.execute('select id, mrid, creationtime, updatetime, closuretime, status, ownedby from OWNPEGPRDDATA.AMA_ADP_VW_IREQ_BASIC');
#for result in cur:
    #ireq,mr,created,updated,closed,status,ownedby=result
    #IREQData.append( ireq,mr,created,updated,closed,status,ownedby)
IREQData=list(cur.fetchall())

cur.close()
con.close()

print "create table"
try:
  cur3.execute("CREATE table IREQ_Status (\
      ID varchar(32),\
      MRID varchar(32),\
      CREATIONTIME date,\
      UPDATETIME date,\
      CLOSURETIME date,\
      STATUS varchar(32),\
      OWNEDBY varchar(64))"); 
  con3.commit()
except:
  print "Error creating table. exists?"

print "write data"
for IREQ in IREQData:
  cur3.execute("insert into IREQ_Status values (?,?,?,?,?,?,?)", IREQ)
con3.commit()
cur3.close()
con3.close()


aproachWriter = None

print 'Done'
