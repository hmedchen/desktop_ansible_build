#!/ama/nsdboard/PRD/PYTHON_Modules/python/bin/python

import cx_Oracle
import MySQLdb
import csv
import sys
import datetime
import os
import json
import sqlite3

        ##########################################################
        #                                                        #
        # Generates a report for all Aproach TRs opened by Pega  #
        #                                                        #
        ##########################################################

SQLiteFile=os.environ["SQLITE_PATH"]+"reporting.sqlite3"

now = datetime.datetime.now()

date=now.strftime('%d-%b-%Y').upper()
nowDir = now.strftime('%d-%b-%Y %H%M').upper()

columns = ('LOGGED_DATE','PURPOSE','WORKTYPE_CLASS','PEGA_WORKOBJECT','RNID','REC_TITLE','PURPOSE+STATUS','NB_OF_HOPS','FIRST_ASSIGNEE','RETURN_DATE','TASK_CATEGORY','RUNTIME')

#if not os.path.exists('G:\\Share\\Data\\Weekly\\'+date+'\\Aproach'):
#    os.makedirs('G:\\Share\\Data\\Weekly\\'+date+'\\Aproach')
#aproachWriter=csv.writer(open('G:\\Share\\Data\\Weekly\\'+date+'\\Aproach\\AllAproach '+nowDir+'.csv','wb'))
#if not os.path.exists('./Aproach'):
#    os.makedirs('./Aproach')
#aproachWriter=csv.writer(open('./Aproach/AllAproach '+nowDir+'.csv','wb'))

# local SQLITE
con3 = sqlite3.connect (SQLiteFile)
cur3 = con3.cursor()

print "get data from Pega"
# Pega Side

#con = cx_Oracle.connect('PEGDEP_CHECK/Dep4#REd2@PEGADB2')
con = cx_Oracle.connect('PEGDEP_CHECK/Dep4#REd2@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.24.33.177)(PORT=31180))(ADDRESS=(PROTOCOL=TCP)(HOST=172.24.33.178)(PORT=31180)))(CONNECT_DATA=(SERVICE_NAME=PEGPRDSRV)))')
cur = con.cursor()

PegaData = {}
AproachData = {}
CombinedData = {}

cur.execute('select a.pxinsname, a.description, w.pxobjclass, a.workobjectid,si.aproachrecordinfoid from OWNPEGPRDDATA.AMA_ADP_PR_INDEX_APROACHINFO si,OWNPEGPRDDATA.AMA_ADP_AI_INFO a, OWNPEGPRDDATA.AMA_ADP_WORK w where si.APROACHINTERACTIONINFOID=a.pxinsname and a.workobjectid=w.pyid')
#cur.execute('select a.pxinsname, a.purpose, w.pxobjclass, a.workobjectid,si.aproachrecordinfoid from AMA_ADP_PR_INDEX_APROACHINFO si,AMA_ADP_AI_INFO a, AmA_ADP_WORK w where si.APROACHINTERACTIONINFOID=a.pxinsname and a.workobjectid=w.pyid')
for result in cur:
    key,purpose,objClass,PEGA,RNID=result
    if (purpose is not None) and (objClass is not None) and (PEGA is not None) and (RNID is not None):
        PegaData[key]=(purpose,objClass,PEGA,RNID)


cur.close()
con.close()

print "get data from Aproach"
# Aproach Side
con2 = MySQLdb.connect(host="172.24.77.29",port=53052,user="sprint_reporting",passwd="q533qPp1Y",db="aprprd")
cur2 = con2.cursor()

cur2.execute('''
  SELECT r.RNID AS RNID, r.title AS REC_TITLE, s.description AS REC_STATUS,
  (select count(hops.id) from ah_tasks hops where hops.RNID=r.RNID and hops.fieldid=27035) - 1 AS nb_of_hops,
    (select fav.name from ah_tasks fa INNER JOIN ap_groups fav ON fav.id=fa.value where fa.RNID=r.RNID and fa.fieldid=27035 limit 0,1) AS first_assignee_group, r.logged_date,
       (select impd.modified_date from ah_tasks impd where impd.RNID=r.RNID and impd.fieldid=27025
          and value in (81, 84, 78, 83, 85, 90, 2431, 82, 91, 2430, 86) limit 0,1) as return_date, r.closed_date, c.description
          AS TASK_CATEGORY FROM ar_records r
          INNER JOIN ar_tasks t ON r.RNID=t.RNID
          INNER JOIN ad_dict_values s ON r.statusid=s.id
          INNER JOIN ad_dict_values c ON t.change_categoryid=c.id
    WHERE r.external_systemid=4 ORDER BY r.RNID
    ''')

for result2 in cur2:
    RNID,titel,status,hops,assignee,loggedDate,returnDate,closedDate,taskCategory=result2
    if returnDate is None:
        runtime = None
    else:
        runtime = returnDate-loggedDate

    AproachData[int(RNID)]=(titel,status,hops,assignee,loggedDate,returnDate,closedDate,taskCategory,runtime)

cur2.close()
con2.close()
print "Assembling data"

for key in PegaData:
    RNID = int(PegaData[key][3])
    if RNID in AproachData:
        if  ((AproachData[RNID])[8]):
            if PegaData[key][0] is None:PegaData[key]
            # to get around null dates
   #         if AproachData[RNID][5] is None:
   #            ReturnDate = 'NULL'
   #         else:
            ReturnDate = AproachData[RNID][5].strftime("%Y-%m-%d")
            CombinedData[key]=(AproachData[RNID][4].strftime("%Y-%m-%d"),PegaData[key][0],PegaData[key][1],PegaData[key][2],
                               PegaData[key][3],AproachData[RNID][0],AproachData[RNID][1],
                               AproachData[RNID][2],AproachData[RNID][3],ReturnDate,AproachData[RNID][7],
                               int(AproachData[RNID][8].total_seconds()/60))
        else:
            CombinedData[key]=(AproachData[RNID][4].strftime("%Y-%m-%d"),PegaData[key][0],PegaData[key][1],PegaData[key][2],
                               PegaData[key][3],AproachData[RNID][0],AproachData[RNID][1],
                               AproachData[RNID][2],AproachData[RNID][3],ReturnDate,AproachData[RNID][7],
                               AproachData[RNID][8])

print "write data"
for key in CombinedData:
  #print json.dumps(CombinedData[key])
  cur3.execute("insert into Aproach values (?,?,?,?,?,?,?,?,?,?,?,?)", CombinedData[key])
#    row_str=', '.join(CombinedData[key])
    
#    aproachWriter.writerow(CombinedData[key])
con3.commit()
cur3.close()
con3.close()


aproachWriter = None

print 'Done'
