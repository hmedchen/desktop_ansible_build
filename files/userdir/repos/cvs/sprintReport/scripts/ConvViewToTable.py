#!/ama/nsdboard/PRD/PYTHON_Modules/python/bin/python

import logging
import sys
import os
import getopt
import datetime
import sqlite3

# GLOBALS

SQLiteFile=os.environ["SQLITE_PATH"]+"reporting.sqlite3"

def ConnectDB():
  global SQLiteFile
  db = sqlite3.connect(SQLiteFile)
  return (db)


def ConvViewToTable (db):
  cursor = db.cursor ()

  print "dropping view_server view;"
  SQL="Drop view view_server;"
  try:
     cursor.execute (SQL)
     print "done"
  except sqlite3.Error as e:
     print "Can't drop view!"
     print "Error: ", e.args[0]

  print "creating view_server as table;"
  SQL="CREATE table view_server AS SELECT Id, IPlan, kpi.FlowId, IReq, Req, kpi.AssetId, CASE WHEN cmdb.Name IS NULL THEN kpi.Name ELSE cmdb.Name END AS Name, Area, Subarea, kpi.Status, CLId, SetId, ApsStatus, Type, Category, Approved, Ordered, App2Ord, Provisioned, Ord2Prv, App2Prv, OSReady, Prv2OS, App2OS, SysReady, OS2Sys, App2Sys, InProd, Sys2Prd, OS2Prd, App2Prd, Exited, Prv2Prd, kpi.CR, MR, MRCreated, MRTitle, MRStatus, RefCR, Cancelled, rlc.LotId, rlc.WishDate, rlc.Tier, CASE WHEN cmdb.OwnedBy IS NULL THEN IREQ_OwnedBy ELSE cmdb.OwnedBy END AS OwnedBy, cmdb.ManagedBy, cmdb.SupportedBy, cmdb.OperatedBy, cmdb.ALCStatus, cmdb.Reason, flow.CR as FlowCR, WorkOrder, ActionType, kpi.App, kpi.Peaks, Level, kpi.Phases, BusinessRequest, flow.Started AS FlowT0, flow.Lastmod AS FlowT1, flow.Status AS FlowStatus, kpi.Name AS Kpiname FROM Pega_ServerKPI AS kpi OUTER LEFT JOIN Cmdb_Server AS cmdb ON kpi.AssetId=cmdb.AssetId OUTER LEFT JOIN Planning_Flow AS flow ON kpi.FlowId=flow.FlowId OUTER LEFT JOIN Pega_Requests AS rlc ON kpi.Req=rlc.RequestId;"
  SQL_I1="CREATE INDEX 'i_exited_inprod' on view_server (Exited ASC, inProd ASC);"
  SQL_I2="CREATE INDEX 'i_inpod_exited' on view_server (inProd ASC, Exited ASC);"
  SQL_I3="CREATE INDEX 'i_owned' on view_server (OwnedBy ASC); "
  SQL_I4="CREATE INDEX 'i_id' on pega_tags (Id ASC);"
  SQL_I5="CREATE INDEX 'i_mr' on view_server (MR ASC);"
  try:
     cursor.execute (SQL)
     print "done"
     print "Creating some indexes..."
     cursor.execute (SQL_I1)
     cursor.execute (SQL_I2)
     cursor.execute (SQL_I3)
     cursor.execute (SQL_I4)
     cursor.execute (SQL_I5)
     print "done"
  except sqlite3.Error as e:
     print "Can't Create table!"
     print "Error: ", e.args[0]

  cursor.close()

#------------------------------------------------------------------------------------------
# MAIN
#
#------------------------------------------------------------------------------------------
def main (argv):
  # open DB connection
  db=ConnectDB()

  ConvViewToTable(db)

  # close DB connection
  db.close()

if __name__ == "__main__":
  main(sys.argv[1:])


