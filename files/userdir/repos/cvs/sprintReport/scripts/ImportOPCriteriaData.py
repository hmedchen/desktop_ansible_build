#!/ama/nsdboard/PRD/PYTHON_Modules/python/bin/python

import logging
import sys
import os
import getopt
import datetime
import urllib2
import ssl
import csv
import json
import sqlite3

import TableData

# init variables from TableData
TableData.init()

# GLOBALS
ValuesTableName="OPCrit_Values"
ValuesLocation="https://adaweb.os.amadeus.net:8443/cgi/opscrit/getData.pl?what=values"

EvaluatedTableName="OPCrit_Evaluated"
EvaluatedLocation="https://adaweb.os.amadeus.net:8443/cgi/opscrit/getData.pl?what=evaluated"

SQLiteFile=os.environ["SQLITE_PATH"]+"reporting.sqlite3"

def ConnectDB():
  global SQLiteFile

  db = sqlite3.connect(SQLiteFile)
  return (db)

def getInsertLine (CSVArray):
  line=[]
  row=''
  #    print CSVArray;
  
  if (type(CSVArray) == dict):
    for key,value in CSVArray.items():
      value=value.encode('string-escape').replace('"', "\'")
      if (value == ''):
        value="NULL"
      elif (value.isdigit() == False):
        value='"'+value+'"'
      if (row != ''):
        row=",".join([row,value])
      else:
        row=value
  else:
    for item in CSVArray:
      item=item.encode('string-escape').replace('"',"\'")
      if (item == ''):
        item="NULL"
      elif (item.isdigit() == False):
        item='"'+item+'"'
      if (row != ''):
        row=",".join([row,item])
      else:
        row=item
  return (row)

def CreateOPEvaluatedTable (db,TabName):
  SQL="CREATE TABLE `" + TabName + "` (\
        `app_name` VARCHAR(50) NOT NULL ,\
        `ts` DATE NOT NULL,\
        `StopStart` INT NULL DEFAULT NULL,\
        `Succ` INT NULL DEFAULT NULL,\
        `Mem` INT NULL DEFAULT NULL,\
        `Cor` INT NULL DEFAULT NULL,\
        `IntErr` INT NULL DEFAULT NULL,\
        `Pria` INT NULL DEFAULT NULL,\
        `Fallb` INT NULL DEFAULT NULL,\
        `Mw` INT NULL DEFAULT NULL,\
        `Auto` INT NULL DEFAULT NULL,\
        `Obj` INT NULL DEFAULT NULL,\
        `Mig` INT NULL DEFAULT NULL,\
        `Sess` INT NULL DEFAULT NULL,\
        `Siz` INT NULL DEFAULT NULL,\
        `NbSet` INT NOT NULL DEFAULT '0',\
        `NbFailed` INT NOT NULL DEFAULT '0',\
        `Conform` DECIMAL\
  )";    

  cursor = db.cursor ()
  print "creating table " +TabName
  try:
     cursor.execute (SQL)
     print "done"
  except sqlite3.Error as e:
     print "Can't create table! Error:", e.args[0]
     
  SQL="create table `" + TabName + "_columns` (\
        crit_name varchar(5),\
        description varchar(50))";

  cursor = db.cursor ()
  print "creating table " + TabName + "_columns"
  try:
     cursor.execute (SQL)
     print "done"
  except sqlite3.Error as e:
     print "Can't create table! Error:", e.args[0]

def CreateOPValuesTable (db,TabName):
  SQL="CREATE TABLE `" + TabName + "` (\
    `ulam_name` VARCHAR(50) NOT NULL ,\
    `ts` DATE NOT NULL ,\
    `average_duration` FLOAT NULL DEFAULT NULL ,\
    `failure` INT NULL DEFAULT NULL ,\
    `success` INT NULL DEFAULT NULL ,\
    `success_rate` FLOAT NULL DEFAULT NULL ,\
    `days` FLOAT NULL DEFAULT NULL ,\
    `nb_core_dump` INT NULL DEFAULT NULL,\
    `warning` INT NULL DEFAULT NULL ,\
    `err` INT NULL DEFAULT NULL ,\
    `critical` INT NULL DEFAULT NULL ,\
    `fatal` INT NULL DEFAULT NULL ,\
    `total_err` INT NULL DEFAULT NULL ,\
    `Volume_trx` INT NULL DEFAULT NULL ,\
    `nb_auto` INT NULL DEFAULT NULL ,\
    `nb_pria` INT NULL DEFAULT NULL ,\
    `nb_release_auto_pria` INT NULL DEFAULT NULL ,\
    `nb_fallback` INT NULL DEFAULT NULL ,\
    `nb_release_fallback` INT NULL DEFAULT NULL ,\
    `mdw_prcgood` FLOAT NULL DEFAULT NULL ,\
    `invalid_log` FLOAT NULL DEFAULT NULL ,\
    `pairing` INT NULL DEFAULT NULL ,\
    `migration` INT NULL DEFAULT NULL ,\
    `max_session` INT NULL DEFAULT NULL ,\
    `max_used_fs_gb` INT NULL DEFAULT NULL ,\
    `max_used_fs_gb_mc` INT NULL DEFAULT NULL \
  )";

  cursor = db.cursor ()
  print "creating table " + TabName
  try:
     cursor.execute (SQL)
     #cursor.execute ("create index xxx on ckwip(kpi);")
     #cursor.execute ("create index yyy on ckwip(Message);")
     print "done"
  except sqlite3.Error as e:
     print "Can't create table! Error:", e.args[0]

  SQL = "create table `" + TabName + "_columns` (\
        crit_name varchar(5),\
        description varchar(50))";

  cursor = db.cursor ()
  print "creating table " + TabName + "_columns"  
  try:
     cursor.execute (SQL)
     #cursor.execute ("create index xxx on ckwip(kpi);")
     #cursor.execute ("create index yyy on ckwip(Message);")
     print "done"
  except sqlite3.Error as e:
     print "Can't create table! Error:", e.args[0]

def CreateAppOwnerTable (db, TabName):
  SQL=" create table " + TabName +"\
      (app_name varchar(3),\
      description varchar(50),\
      SMU varchar(3),\
      OwnedBy varchar(11))";

  cursor = db.cursor ()
  print "creating table " +TabName
  try:
     cursor.execute (SQL)
     print "done"
  except sqlite3.Error as e:
     print "Can't create table! Error:", e.args[0]
     
def InitValuesTable (db,TabName):
  cursor = db.cursor ()
  print "inserting into table " + TabName + "_columns"
  try:
     for line in TableData.ValuesTableColumns:
        SQL="insert into " + TabName + "_columns values " + line
        cursor.execute (SQL)
     db.commit()
     print "done"
  except sqlite3.Error as e:
     print "Can't insert into table! Error:", e.args[0]

def InitEvaluatedTable (db,TabName):
  cursor = db.cursor ()
  print "inserting into table " + TabName + "_columns"
  try:
     for line in TableData.ValuesEvaluatedColumns:
        SQL="insert into " + TabName + "_columns (crit_name, description) values " + line
        print SQL
        cursor.execute (SQL)
     db.commit()
     print "done"

#  print "SQL Statement: " + SQL + "\n"
  except sqlite3.Error as e:
     print "Can't insert into table! Error:", e.args[0]
  
def InitAppOwnerTable (db,TabName):
  cursor = db.cursor ()
  print "inserting into table " + TabName
  try:
     for line in TableData.AppDetails:
        SQL="insert into " + TabName + " values " + line
        cursor.execute (SQL)
     db.commit()
     print "done"
  except sqlite3.Error as e:
     print "Can't insert into table! Error:", e.args[0]
#------------------------------------------------------------------------------------------
# Import CSV File
# TODO: SmartInsert checks each row and inserts only the updates on a given key
#------------------------------------------------------------------------------------------
def ImportFile (db, RemoteFileName, TableName):
  str=","
  InsertData=''
  lines=[]
   
  ctx = ssl.create_default_context()
  ctx.check_hostname = False
  ctx.verify_mode = ssl.CERT_NONE
 
  fp=urllib2.urlopen(RemoteFileName,context=ctx)
  cursor = db.cursor ()

  # Get rid of header line
  columns=fp.readline()
  #print columns
  RemoteCSVFile = csv.reader(fp,  delimiter=',', quotechar='"')
  for CSVArray in RemoteCSVFile:
    line=getInsertLine(CSVArray)
    lines.append("("+line.strip()+")\n")
  try:
    try:
      print "Trying to truncate table"
      # clean old table
      print "delete from "+TableName
      cursor.execute ("delete from "+TableName+";")
    except sqlite3.OperationalError as err:
      print "Error Truncating - continuing"+format(err)
    print "select count(*) count from "+TableName
    cursor.execute ("select count(*) count from "+TableName+";")
    row = cursor.fetchone ()
    #print row[0]
    InsertData=str.join(lines)
    print "insert data"
    # import data into the table
    # can't use that as the WIP excel has dupe column names
    for line in lines:
      SQL_Insert="INSERT INTO "+TableName+" VALUES "+line+";"
      #print SQL_Insert
      cursor.execute (SQL_Insert)
    print "committing"
    db.commit()
  except sqlite3.OperationalError as err:
    #_mysql_exceptions.OperationalError
    print("Something went wrong: {}"+format(err)) 
      
     
  # validating row count
  cursor.execute ("select count(*) from "+TableName)
  row = cursor.fetchone ()
  print row[0]
  
  fp.close()
#  print InsertData;
   
  
#  print row
  cursor.close ()
  
#------------------------------------------------------------------------------------------
# MAIN
#
#------------------------------------------------------------------------------------------
def main (argv):
  global ValuesTableName
  global ValuesLocation
  global EvaluatedTableName
  global EvaluatedLocation

  # open DB connection
  db=ConnectDB();

  # reading CKWIP source file
  print "----- OPCriteria Values ----"
  CreateOPValuesTable(db,ValuesTableName)
  CreateOPEvaluatedTable(db,EvaluatedTableName)
  CreateAppOwnerTable(db,"AppResponsible")

  InitValuesTable (db, ValuesTableName)
  InitEvaluatedTable (db, EvaluatedTableName)
  InitAppOwnerTable(db,"AppResponsible")

  ImportFile (db, ValuesLocation, ValuesTableName)  
  ImportFile (db, EvaluatedLocation, EvaluatedTableName)  

  # close DB connection
  db.close()

if __name__ == "__main__":
  main(sys.argv[1:])


