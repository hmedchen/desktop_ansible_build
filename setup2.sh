#!/usr/bin/env bash
echo "------- running ansible playbooks -----"
ansible-playbook -v playbooks/main.yml
ansible-playbook -v playbooks/atom.yml
ansible-playbook -v playbooks/tilix.yml
ansible-playbook -v playbooks/repos.yml

